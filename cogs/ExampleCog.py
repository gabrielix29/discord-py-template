from discord.ext import commands


class ExampleCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='hello')
    async def hello(self, ctx):
        await ctx.send('hello ' + ctx.author.mention)


def setup(bot):
    bot.add_cog(ExampleCog(bot))
